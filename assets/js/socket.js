// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/web/endpoint.ex":
import {Socket} from "phoenix"

let socket = new Socket("/socket", {params: {token: window.userToken}})

socket.connect()

let message = document.getElementById("message")
let nickName = document.getElementById("name")
let msg_list = document.getElementById("msg-list")

// Now that you are connected, you can join channels with a topic:
let channel = socket.channel("room:timeline", {})
channel.join()
  .receive("ok", resp => { console.log("Joined successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })

message.focus();
message.addEventListener("keypress", event => {
  if(event.keyCode == 13) {
    channel.push('message:new', {message: event.target.value, user: nickName.value})
    console.log(nickName.value, event.target.value)
    event.target.value = ""
  }
})
channel.on('message:new', payload => {
  let template = document.createElement("li")
  template.className = payload.user == nickName.value ? "mine" : "yours"
  template.innerHTML = `<b>${payload.user}</b>: ${payload.message}`
  msg_list.appendChild(template);
  msg_list.scrollTop = msg_list.scrollHeight;
})

export default socket
